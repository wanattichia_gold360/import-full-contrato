var sql_server = require('./controller/sql_server');
var my_sql = require('./controller/my_sql');

processa();

function processa() {
    sql_server.select(function (rows) {
        processaContrato(0, rows);
    });
}

function processaContrato(i, contratos) {
    if (contratos.length > 0) {
        my_sql.init(contratos[i], function (sucesso, mensagem) {
            console.log('loop: ' + i);

            i++;
            if (i < contratos.length)
                processaContrato(i, contratos);
            else{
                contratos = null;
                sql_server.update(function (sucesso) {
                    processa();
                });
            }
        });
    }
}