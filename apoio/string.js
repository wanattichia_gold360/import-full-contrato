module.exports = {

    format: function (stringValor, arrayParamentros) {
        for (var i = 0; i < arrayParamentros.length; i++) {
            stringValor = stringValor.replace('{' + i + '}', arrayParamentros[i]);
        }

        return stringValor;
    },

    padLeft: function (valor, l, c) {
        return valor.padLeft(l, c);
    },

    isNullOrEmpty: function (valor) {
        return valor == null || valor == undefined || String.toString(valor).trim() == '';
    }
};

String.prototype.padLeft = function (l, c) {
    return this.length < l ? Array(l - this.length + 1).join(c || " ") + this : this;
}