var String = require('../apoio/string');

module.exports = {
    Now: function () {
        var data = new Date();
        return String.format('{0}-{1}-{2} {3}:{4}:{5}', [data.getFullYear(), data.getMonth(), data.getDay(), data.getHours(), data.getMinutes(), data.getSeconds()]);
    },

    Today: function () {
        var data = new Date();
        return String.format('{0}-{1}-{2}', [data.getFullYear(), data.getMonth(), data.getDay()]);
    },

    Convert: function (dataStr, horaStr) {
        return String.format('{0} {1}:00:00', [dataStr, String.padLeft(horaStr.substr(0, 2), 2, '0')]);
    }
};