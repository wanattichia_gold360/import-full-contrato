module.exports = {

    notContains: function (arr, value) {
        var retorno = [];

        value = value.toLowerCase();
        for (var i = 0; i < arr.length; i++){
            if (arr[i].toLowerCase().indexOf(value) == -1)
                retorno[retorno.length] = arr[i];
        }

        return retorno;
    },

    maxTime: function (array) {
        if (String.isNullOrEmpty(array)) array = [];

        var arr = [];
        for (var i = 0; i < array.length; i++){
            arr[arr.length] = array[i].time;
        }

        return arr.length == 0 ? 0 : arr.length == 1 ? arr[0] : Math.max.apply(Math, arr);
    }
};