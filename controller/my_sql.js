var module_mysql = require('mysql');
var DateTime = require('./apoio/datetime');
var String = require('./apoio/string');

var mysql = {
    con: function () {
        return module_mysql.createConnection({
            host: 'gold-360-base.cl769wud4zzi.us-east-1.rds.amazonaws.com',
            user: 'gold360',
            password: 'C0a6r9s3d7',
            database: 'gold360'
        });
    },

    open: function () {
        mysql.con.connect();
    },

    quit: function (con) {
        con.end();
    },

    canal: function (nome, call) {

        nome = nome.toUpperCase().trim();

        var con =  mysql.con();
        con.query('SELECT id_canal FROM tb_canal WHERE nome_canal = ?',[nome], function(err,res){
            if (res.length == 0){
                con.query('INSERT INTO tb_canal(nome_canal, data_criacao_canal) VALUES(?, ?)',[nome, DateTime.Now()], function(err,res){
                    mysql.quit(con);
                    if (err) { throw 'erro insert canal'; } else { mysql.canal(nome, call); }
                });
            } else {
                mysql.quit(con);
                if (err) { throw 'erro select canal'; } else { call(res[0].id_canal); }
            }
        });
    },

    keyword: function (nome, call) {

        nome = nome.toUpperCase().trim();

        var con =  mysql.con();
        con.query('SELECT id_keyword FROM tb_keyword WHERE nome_keyword = ?',[nome], function(err,res){
            if (res.length == 0){
                con.query('INSERT INTO tb_keyword(nome_keyword, data_criacao_keyword) VALUES(?, ?)',[nome, DateTime.Now()], function(err,res){
                    mysql.quit(con);
                    if (err) { throw 'erro insert keyword'; } else { mysql.keyword(nome, call); }
                });
            } else {
                mysql.quit(con);
                if (err) { throw 'erro select keyword'; } else { call(res[0].id_keyword); }
            }
        });
    },

    produto: function (nome, call) {

        nome = nome.toUpperCase().trim();

        var con =  mysql.con();
        con.query('SELECT id_produto FROM tb_produto WHERE nome_produto = ?',[nome], function(err,res){
            if (res.length == 0){
                con.query('INSERT INTO tb_produto(nome_produto, data_criacao_produto) VALUES(?, ?)',[nome, DateTime.Now()], function(err,res){
                    mysql.quit(con);
                    if (err) { throw 'erro insert produto'; } else { mysql.produto(nome, call); }
                });
            } else {
                mysql.quit(con);
                if (err) { throw 'erro select produto'; } else { call(res[0].id_produto); }
            }
        });
    },

    contrato: function (contratoStr, call) {

        var query = 'SELECT ' +
            ' c.id_contrato ' +
            ',c.msisdn ' +
            ',c.data_e_hora_ativacao ' +
            ',c.data_e_hora_cancelamento ' +
            ',c.id_keyword ' +
            ',c.id_canal ' +
            ',c.id_produto ' +
            ',c.data_da_proxima_cobranca ' +
            ',c.trial ' +
            'FROM tb_contrato c ' +
            'INNER JOIN tb_keyword k ON k.id_keyword = c.id_keyword ' +
            'INNER JOIN tb_canal cn ON cn.id_canal = c.id_canal ' +
            'INNER JOIN tb_produto p ON p.id_produto = c.id_produto ' +
            'WHERE c.msisdn = ? '  +
            'AND c.id_produto = ? ';

        mysql.produto(contratoStr.PRODUCT_NAME, function (id_produto) {
            var con =  mysql.con();
            con.query(query, [contratoStr.ID_MSISDN, id_produto], function(err,res){
                mysql.quit(con);
                if (err) { throw 'erro select contrato'; }
                else {
                    mysql.save(res.length > 0 ? res[0] : null, id_produto, contratoStr, call);
                }
            });
        });
    },

    save: function (contrato, id_produto, contratoStr, call) {

        mysql.canal(contratoStr.ID_COMMAND, function (id_canal) {
            mysql.keyword(contratoStr.DE_SOURCE, function (id_keyword) {

                if (contrato != null){
                    mysql.update(contrato, id_canal, id_keyword, contratoStr, call);
                } else {
                    mysql.insert(id_canal, id_keyword, id_produto, contratoStr, call);
                }
            });
        });
    },

    insert: function (id_canal, id_keyword, id_produto, contratoStr, call) {

        var ativacao = contratoStr.ID_EVENT == 1;
        var dataRegistro = DateTime.Convert(contratoStr.DT_DAYPROCESS, contratoStr.DT_HOURPROCESS)
        var contrato = [
            contratoStr.ID_MSISDN, // msisdn
            ativacao ? dataRegistro : null, // data_e_hora_ativacao
            ativacao ? null : dataRegistro, // data_e_hora_cancelamento
            id_keyword, // id_keyword
            id_canal, // id_canal
            id_produto, // id_produto
            null, // data_da_proxima_cobranca
            contratoStr.DE_SUBSCRIPTIONTYPE == 'Trial' // trial
        ];

        var query = 'INSERT INTO tb_contrato(msisdn,data_e_hora_ativacao,data_e_hora_cancelamento,id_keyword,id_canal,id_produto,data_da_proxima_cobranca,trial) VALUES(?,?,?,?,?,?,?,?)';
        var con =  mysql.con();
        con.query(query,contrato, function(err,res){
            mysql.quit(con);
            if (err) { call(false, 'erro insert contrato'); }
            else {
                call(true, 'sucesso');
            }
        });
    },

    update: function (contrato, id_canal, id_keyword, contratoStr, call) {

        if (contratoStr.ID_EVENT == 1)
            contrato.data_e_hora_ativacao = DateTime.Convert(contratoStr.DT_DAYPROCESS, contratoStr.DT_HOURPROCESS);
        else
            contrato.data_e_hora_cancelamento = DateTime.Convert(contratoStr.DT_DAYPROCESS, contratoStr.DT_HOURPROCESS);

        contrato.id_keyword = id_keyword;
        contrato.id_canal = id_canal;
        contrato.trial = contratoStr.DE_SUBSCRIPTIONTYPE == 'Trial';

        contrato = [
            contrato.data_e_hora_ativacao,
            contrato.data_e_hora_cancelamento,
            contrato.id_keyword,
            contrato.id_canal,
            contrato.trial,
            contrato.id_contrato
        ];

        var query = 'UPDATE tb_contrato SET data_e_hora_ativacao = ?, data_e_hora_cancelamento = ?, id_keyword = ?, id_canal = ?, trial = ? WHERE id_contrato = ?';
        var con =  mysql.con();
        con.query(query,contrato, function(err,res){
            mysql.quit(con);
            if (err) { call(false, 'erro update contrato'); }
            else {
                call(true, 'sucesso');
            }
        });
    }
};

module.exports = {
    init: mysql.contrato
};